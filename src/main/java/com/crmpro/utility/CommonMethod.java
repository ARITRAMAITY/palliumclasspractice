package com.crmpro.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class CommonMethod {
	
	public String property_file_reading(String keyrequired) throws IOException 
	{
		File file = new File("./TestData/Data.properties");
		FileInputStream fi = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fi);
		return prop.getProperty(keyrequired);
		
	}

}


