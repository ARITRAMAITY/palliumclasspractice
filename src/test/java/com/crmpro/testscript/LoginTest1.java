package com.crmpro.testscript;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.crmpro.pages.LoginPage;

import jxl.read.biff.BiffException;

public class LoginTest1 {
	
	WebDriver driver;
	@BeforeMethod
	public void BrowserAppLaunch() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			driver = new ChromeDriver(); //Interview Question What is web driver?
			                                     //What is driver and ChromeDriver?

			Thread.sleep(2000);

			driver.manage().window().maximize();
			Thread.sleep(2000);
			driver.get("http://newtours.demoaut.com/");
		}
		
     @Test(enabled = false)
     public void verifyValidLogin() throws InterruptedException   
     	{  int flag=0;
     		WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
     		username.sendKeys("dasd");
     		//WebElement password=driver.findElement(By.name("password"));
     		WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
     		password.sendKeys("dasd");
     		//WebElement signin=driver.findElement(By.name("login"));
     		WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
     		signin.click();
    
     		Thread.sleep(2000);//validet Login;
     		String expectedtitle ="Find a Flight: Mercury Tours:";
     		String actualtitle1=driver.getTitle();
     		if (expectedtitle.equals(actualtitle1))
     		{
     			flag=1;
     		}
     		Assert.assertEquals(flag, 1);
     		Reporter.log("pass");
     		}
     		@Test(enabled = false)
     		public void verifyLoginwithCorrectUsernameandPasswordWrong() throws InterruptedException

     		{int flag = 0;
     		WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
     		username.sendKeys("dasd");
     		//WebElement password=driver.findElement(By.name("password"));
     		WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
     		password.sendKeys("@#$%");
     		WebElement signin=driver.findElement(By.name("login"));
     		//WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
     		signin.click();

     		String expectedtitle ="Find a Flight: Mercury Tours:";
     		String actualtitle1=driver.getTitle();
     		if (expectedtitle.equals(actualtitle1))
     		{
     			flag=1;
     		}
     		Assert.assertEquals(flag, 1);
     		Reporter.log("pass");
     		}

     		@Test(enabled = false)
     		public void verifyLoginwithWrongUsernameandPasswordcorrect() throws InterruptedException

     		{int flag = 0;
     		WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
     		username.sendKeys("1234");
     		//WebElement password=driver.findElement(By.name("password"));
     		WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
     		password.sendKeys("dasd");
     		WebElement signin=driver.findElement(By.name("login"));
     		//WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
     		signin.click();

     		String expectedtitle ="Find a Flight: Mercury Tours:";
     		String actualtitle1=driver.getTitle();
     		if (expectedtitle.equals(actualtitle1))
     		{
     			flag=1;
     		}
     		Assert.assertEquals(flag, 1);
     		Reporter.log("pass");
     		}
     		@Test(enabled = false)
     		public void verifyLoginwithWrongUsernameandPasswordWrong() throws InterruptedException

     		{int flag = 0;
     		WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
     		username.sendKeys("1234");
     		//WebElement password=driver.findElement(By.name("password"));
     		WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
     		password.sendKeys("@#$%");
     		WebElement signin=driver.findElement(By.name("login"));
     		//WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
     		signin.click();

     		String expectedtitle ="Find a Flight: Mercury Tours:";
     		String actualtitle1=driver.getTitle();
     		if (expectedtitle.equals(actualtitle1))
     		{
     			flag=1;
     		}
     		Assert.assertEquals(flag, 1);
     		Reporter.log("pass");
     		}
     		@Test(enabled = false)
     		public void verifyLoginwithUsernameGivenPasswordBlank() throws InterruptedException

     		{int flag = 0;
     		WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
     		username.sendKeys("dasd");
     		//WebElement password=driver.findElement(By.name("password"));
     		WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
     		password.sendKeys("");
     		WebElement signin=driver.findElement(By.name("login"));
     		//WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
     		signin.click();

     		String expectedtitle ="Find a Flight: Mercury Tours:";
     		String actualtitle1=driver.getTitle();
     		if (expectedtitle.equals(actualtitle1))
     		{
     			flag=1;
     		}
     		Assert.assertEquals(flag, 1);
     		Reporter.log("pass");
     		}
     		@Test(enabled = false)
     		public void verifyLoginwithUsernameBlankPasswordGiven() throws InterruptedException

     		{int flag = 0;
     		WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
     		username.sendKeys("");
     		//WebElement password=driver.findElement(By.name("password"));
     		WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
     		password.sendKeys("1234");
     		WebElement signin=driver.findElement(By.name("login"));
     		//WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
     		signin.click();

     		String expectedtitle ="Find a Flight: Mercury Tours:";
     		String actualtitle1=driver.getTitle();
     		if (expectedtitle.equals(actualtitle1))
     		{
     			flag=1;
     		}
     		Assert.assertEquals(flag, 1);
     		Reporter.log("pass");

     		}

     		@Test(enabled = false)
     		public void verifyLoginwithUsernameBlankPasswordBlank() throws InterruptedException

     		{int flag = 0;
     		WebElement username=driver.findElement(By.xpath("//input[@name='userName']"));
     		username.sendKeys("");
     		//WebElement password=driver.findElement(By.name("password"));
     		WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
     		password.sendKeys("");
     		WebElement signin=driver.findElement(By.name("login"));
     		//WebElement signin=driver.findElement(By.xpath("//input[@name='login']"));
     		signin.click();

     		String expectedtitle ="Find a Flight: Mercury Tours:";
     		String actualtitle1=driver.getTitle();
     		if (expectedtitle.equals(actualtitle1))
     		{
     			flag=1;
     		}
     		Assert.assertEquals(flag, 1);
     		Reporter.log("pass");
     		}


     		//if (expectedtitle.equals(actualtitle1))
     		//{
     		  //System.out.println("pass");
     		//}
     		//else
     		//{
     		 // System.out.println("fail");
     		//}
     		
     		@Test
     		public void verifyLoginViaExcel() throws BiffException, IOException, InterruptedException
     		{
     			LoginPage login_page=PageFactory.initElements(driver, LoginPage.class);
     			login_page.excelFileReading("Login_Data", null, null);
     		}
     		
    
     		
     	
	
     @AfterMethod
     public void driverClose()
     {
    	 driver.quit();
     }
}
